const mongoose = require('mongoose')

mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true)
mongoose.connect('mongodb://localhost/noderest', { useNewUrlParser: true })
mongoose.Promise = global.Promise

module.exports = mongoose